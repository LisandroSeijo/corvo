<?php
/**
 * Directorio del core
 */
$C_PATH = dirname(__FILE__);


/**
 * Variables de session
 */
session_start();


/**
 * Inicio de las configuraciones
 */
include "$C_PATH/start_config.php";


/**
 * Incluye las librerias, modelos, las vistas y los controladores
 */
if (!include_path($C_PATH_LIB, true)
	|| !include_path($C_PATH_MODELO)
	|| !include_path($C_PATH_VISTA)
	|| !include_path($C_PATH_CONTROL))
	die('Error al cargar las librerias');


/**
 * Inicialización las clases estáticas
 */
include "$C_PATH/static.php";
?>