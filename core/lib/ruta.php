<?php
class ruta
{
	/**
	 * Instancia de la clase
	 * 
	 * @var ruta
	 */
	static private $instancia;
	
	/**
	 * Arreglo que contiene los parámetros de la url separados por barra (/)
	 * 
	 * @var array
	 * @access private
	 */
	static private $params = array();
	
	/**
	 * URL de la página
	 * 
	 * @var string
	 * @access private
	 */
	static private $url;
	
	/**
	 * Ruta donde se encuentra el index de la página
	 * 
	 * @var string
	 * @access private
	 */
	static private $pagina;
	
	/**
	 * Ruta donde se encuentra actualmente
	 * 
	 * @var string
	 * @access private
	 */
	static private $ruta;
	
	/**
	 * Ruta donde se encuentra actualmente sin barra (/) al final
	 * @var unknown
	 */
	static private $ruta_purg;
	
	/**
	 * La ruta relativa donde se encuentra el index
	 * 
	 * @var string
	 * @access private
	 */
	static private $ruta_index;
	
	
	/**
	 * Constructor
	 */
	function __construct()
	{
		self::$cargar_datos();
	}
	
	
	/**
	 * Inicializa todas las variables
	 */
	private function cargar_datos()
	{
		self::setURL();
		
		self::setRutaIndex();
		
		self::setRuta();
		
		self::setPagina();
		
		self::setParams();
	}
	
	
	/**
	 * Setea la url
	 */
	static private function setURL()
	{
		self::$url = 'http://'.$_SERVER['HTTP_HOST'].'/';
	}
	
	
	/**
	 * Obtiene la ruta donde esta situado el index
	 * 
	 * Si esta en el directorio raiz $ruta_index queda vacio
	 * 
	 * Si esta en un directorio como pagina independiente devuelve el directorio
	 * Ej: www.mipagina.com/pagina_independiente/
	 * $ruta_index: /pagina_independiente
	 */
	static private function setRutaIndex()
	{
		$dir = $_SERVER['PHP_SELF'];
	
		if (substr($dir,strlen($dir)-1,1) == '/')
			$dir = substr($dir, 0,strlen($dir)-1);
	
		$dir = str_replace('/index.php', '', $dir);
	
		self::$ruta_index = $dir;
	}
	
	
	/**
	 * Setea la ruta por la que accedió ($_SERVER['REQUEST_URI'])
	 */
	static private function setRuta()
	{
		/*
		 * Ruta completa, incluye en la cadena si es una pagina aparte dentro de un directorio
		 */
		self::$ruta = $_SERVER['REQUEST_URI'];
		
		
		/*
		 * Ruta completo sin las barras (/) al principio ni al final, incluye en la cadena si es una pagina aparte dentro de un directorio
		 */
		self::$ruta_purg = self::$ruta;
		
		if (substr(self::$ruta,0,1) == '/')
			self::$ruta_purg = substr(self::$ruta_purg,1);
		
		if (substr(self::$ruta_purg, strlen(self::$ruta_purg)-1,1) == '/')
			self::$ruta_purg = substr(self::$ruta_purg, 0,strlen(self::$ruta_purg)-1);
	}
	
	
	/**
	 * Setea la pagina, si el index esta en un directorio como pagina aparte lo incluye
	 */
	static private function setPagina()
	{
		if (empty(self::$ruta_index))
			self::$pagina = self::$url;
		
		else
			self::$pagina = 'http://'.$_SERVER['HTTP_HOST'].self::$ruta_index.'/';
	}
	
	
	/**
	 * Setea los parametros, si el index esta en un subdirectorio como pagina parte 
	 * no lo toma como parametro
	 * 
	 * 
	 * Si un parámetros tiene variables GET no se tienen en cuenta
	 * Ej: la ruta http://mipagina.com/seccion?pag=2
	 * El primer parámetro es "seccion" y no "seccion?pag=2"
	 * 
	 * 
	 * Ej: www.mipagina.com/pagina_independiente/seccion/articulo
	 * Parametros: 2
	 * Parametro 1: seccion
	 * Parametro 2: articulo
	 */
	static private function setParams()
	{
		$dir = $_SERVER['PHP_SELF'];
		

		//Le quita la barra final
		if (substr($dir,strlen($dir)-1,1) == '/')
			$dir = substr($dir, 0,strlen($dir)-1);
		
		
		//Le quita index.php y la ruta para que queden solo los parámetros
		$dir = str_replace('/index.php', '', $dir);
		$uri = str_replace($dir, '', self::$ruta);
		
		
		//Le quita la barra (/) al principio y al final
		if (substr($uri,0,1) == '/')
			$uri = substr($uri,1);

		if (substr($uri, strlen($uri)-1,1) == '/')
			$uri = substr($uri, 0,strlen($uri)-1);
		

		$array_uri = explode('/', $uri);
		
		if (count($array_uri) == 1 && $array_uri[0] == '') 
			return;

		
		foreach($array_uri as $param)
		{
			$p = $param;
			
			//Separa el parámetro de las variables GET
			if (strpos($param, '?'))
			{
				$exp = explode('?', $param);
				$p = $exp[0];
			}

			self::$params[] = $p;
		}
	}
	
	
	/*
	 * Métodos GET
	 */
	/**
	 * Devuelve la url de la página (http://miweb.com)
	 * 
	 * @return string
	 */
	static function getUrl()
	{
		if (empty(self::$url))
			self::cargar_datos();

		return self::$url;
	}
	
	
	/**
	 * Devuelve la ruta absoluta donde está el index
	 * 
	 * @return string
	 */
	static function getPagina()
	{
		if (empty(self::$pagina))
			self::cargar_datos();

		return self::$pagina;
	}
	
	
	/**
	 * Devuelve la ruta relativa del index
	 * 
	 * @return string
	 */
	static function getRutaIndex()
	{
		if (empty(self::$ruta_index))
			self::cargar_datos();

		return self::$ruta_index;
	}
	
	
	/**
	 * Devuelve la ruta completa donde se encuentra actualmente
	 * 
	 * @return string
	 */
	static function getRuta()
	{
		if (empty(self::$ruta))
			self::cargar_datos();

		return self::$ruta;
	}
	
	
	/**
	 * Devuelve la ruta completa donde se encuentra actualmente sin la barra (/) al final
	 * 
	 * @return string
	 */
	static function getRutaPurg()
	{
		if (empty(self::$ruta_purg))
			self::cargar_datos();

		return self::$ruta_purg;
	}
	
	
	/**
	 * Devuelve la cantidad de parámetros separados por una barra (/)
	 * 
	 * @return number
	 */
	static function getCountParams()
	{
		if (empty(self::$params))
			self::cargar_datos();

		return count(self::$params);
	}
	
	
	/**
	 * Devuelve un arreglo con todos los parámetros
	 * 
	 * @return array
	 */
	static function getParams()
	{
		if (empty(self::$params))
			self::cargar_datos();

		return self::$params;
	}
	
	
	/**
	 * Devuelve un parámetro de la ruta donde se encuentra
	 * 
	 * @param number $x
	 * @return mixed
	 */
	static function getParam($x)
	{
		if ($x > self::getCountParams() || $x < 0)
			return 0;
		
		return self::$params[$x];
	}
}

?>