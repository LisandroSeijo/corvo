<?php
/**
 * Devuelve si una variable no está seteada o vacia
 * 
 * @param mixed $var
 * @return boolean
 */
function empty_set($var)
{
	$ret = true;
	
	if (!isset($var) || empty($var))
		$ret = false;
	
	return $ret;
}
?>