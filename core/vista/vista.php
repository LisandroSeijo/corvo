<?php
/**
 * Clase de manejo de las vistas
 */
global $C_PATH_VISTA;

define('SMARTY_TEMPLATES', "$C_PATH_VISTA/plantillas/Smarty/templates/");
define('SMARTY_TEMPLATES_C', "$C_PATH_VISTA/plantillas/Smarty/templates_c/");
define('SMARTY_CONFIGS', "$C_PATH_VISTA/plantillas/Smarty/configs/");
define('SMARTY_CACHE', "$C_PATH_VISTA/plantillas/Smarty/cache/");

require_once "$C_PATH_VISTA/plantillas/Smarty/libs/Smarty.class.php";


class Vista
{	
	/**
	 * Directorio de las vistas
	 * @var string
	 * @access private
	 */
	private $path;
	
	
	/**
	 * Cambiar esto para que se pueda seleccionar otras plantillas
	 * @var Object Smarty
	 * @access private
	 */
	private $plantilla;
	
	
	function __construct()
	{
		global $path_vista;
		global $C_PATH_VISTA;

		$this->path = $path_vista;
		$this->plantilla = new Smarty();

		$this->plantilla->template_dir = SMARTY_TEMPLATES;
		$this->plantilla->compile_dir = SMARTY_TEMPLATES_C;
		$this->plantilla->config_dir = SMARTY_CONFIGS;
		$this->plantilla->cache_dir = SMARTY_DIR;
	}
	
	
	/**
	 * Muestra una vista
	 * 
	 * @param string $vista
	 * @param array $vars
	 * @param boolean $plantilla
	 */
	function cargar($vista, $vars=NULL, $plantilla=NULL)
	{	
		$vista = "{$this->path}/$vista";
		
		if (!is_file($vista))
			return 0;
		
		if (is_array($vars))
			$this->cargar_vars_smarty($vars);
		
		$this->plantilla->display($vista);
	}
	
	
	/**
	 * Carga las variables en Smarty
	 * @param array $vars
	 */
	function cargar_vars_smarty($vars)
	{
		foreach ($vars as $k => $v)
		{
			if (is_object($v))
				$this->plantilla->assignByRef($k, $v);
			else
				$this->plantilla->assign($k, $v);
		}
	}
}

?>