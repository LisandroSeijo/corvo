<?php
/**
 * Setea los directorios del core
 * 
 * Estos directorios son solamente para el core y no se usan en la aplicación
 */
//Directorio del config
$C_PATH_CONFIG = "$C_PATH/config";
if (!is_dir($C_PATH_CONFIG)) 
{
	die('La carpeta de la configuracion del sistema no existe');
}


//Directorio de la libreria del controlador del core
$C_PATH_CONTROL = "$C_PATH/control";
if (!is_dir($C_PATH_CONTROL))
{
	die('La carpeta del controlador del sistema no existe');
}


//Directorio de las librerias del modelo
$C_PATH_MODELO = "$C_PATH/modelo";

if (!is_dir($C_PATH_MODELO))
{
	die('La carpeta de los modelos del sistema no existe');
}


//Directorio de la libreria de la vista del core
$C_PATH_VISTA = "$C_PATH/vista";
if (!is_dir($C_PATH_VISTA))
{
	die('La carpeta de la vista del sistema no existe');
}


//Directorio de la libreria
$C_PATH_LIB = "$C_PATH/lib";
if (!is_dir($C_PATH_LIB))
{
	die('La carpeta de las librerias del sistema no existe');
}



include "$C_PATH_LIB/includes.php";



//Incluye las configuraciones
include_path($C_PATH_CONFIG, true);



/*
 * Carga como va a mostrar los errores
 */
$errors = get_config_index('C_SYSTEM_ERROR');
if ($errors && get_config_activo())
{
	$reporting = '';
	$noreporting = '';

	if ($errors['E_ALL'] === 1)
	{
		$reporting = 'E_ALL';
		$noreporting = '';
		
		foreach($errors as $k => $v)
		{
			if ($v === 0)
			{
				if (empty($noreporging))
					$noreporting .= $k;
				else
					$noreporging .= " | $k";
			}
		}
		
		if (!empty($noreporting))
			$reporting .= " ^ ($noreporting)";
	}
	
	else
	{	
		foreach($errors as $k => $v)
		{
			if ($v === 1)
			{
				if (empty($reporting))
					$reporting .= $k;
				else
					$reporting .= " | $k";
			}
			
			else if ($v === 0)
			{
				if (empty($noreporting))
					$noreporting .= $k;
				else
					$noreporting .= " | $k";
			}
			
			if (!empty($reporting))
			{
				if (!empty($noreporting))
					$reporting .= " ^ ($noreporting)";
			}
		}
	}
	
	if (!empty($reporting))
		error_reporting($reporting);
}
?>