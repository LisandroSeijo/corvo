<?php
/**
 * Configuración de errores que va a mostrar php 
 * 
 * (ver función error_reporting() @link http://php.net/manual/es/function.error-reporting.php)
 * 
 * E_ERROR: 			Errores Fatales en tiempo de ejecución. Éstos indican errores que no se pueden recuperar, tales como un problema de asignación de memoria. La ejecución del script se interrumpe.
 * E_WARNING: 			Advertencias en tiempo de ejecución (errores no fatales). La ejecución del script no se interrumpe.
 * E_PARSE: 			Errores de análisis en tiempo de compilación. Los errores de análisis deberían ser generados únicamente por el analizador.
 * E_NOTICE: 			Avisos en tiempo de ejecución. Indican que el script encontró algo que podría señalar un error, pero que también podría ocurrir en el curso normal al ejecutar un script.
 * E_CORE_ERROR: 		Errores fatales que ocurren durante el arranque incial de PHP. Son como un E_ERROR, excepto que son generados por el núcleo de PHP.
 * E_CORE_WARNING: 		Advertencias (errores no fatales) que ocurren durante el arranque inicial de PHP. Son como un E_WARNING, excepto que son generados por el núcleo de PHP.
 * E_COMPILE_ERROR: 	Errores fatales en tiempo de compilación. Son como un E_ERROR, excepto que son generados por Motor de Script Zend.
 * E_COMPILE_WARNING: 	Advertencias en tiempo de compilación (errores no fatales). Son como un E_WARNING, excepto que son generados por Motor de Script Zend.
 * E_USER_ERROR: 		Mensaje de error generado por el usuario. Es como un E_ERROR, excepto que es generado por código de PHP mediante el uso de la función de PHP trigger_error().
 * E_USER_WARNING: 		Mensaje de advertencia generado por el usuario. Es como un E_WARNING, excepto que es generado por código de PHP mediante el uso de la función de PHP trigger_error().
 * E_USER_NOTICE: 		Mensaje de aviso generado por el usuario. Es como un E_NOTICE, excepto que es generado por código de PHP mediante el uso de la función de PHP trigger_error().
 * E_STRICT: 			Habilítelo para que PHP sugiera cambios en su código, lo que asegurará la mejor interoperabilidad y compatibilidad con versiones posteriores de PHP de su código.
 * E_RECOVERABLE_ERROR: Error fatal capturable. Indica que ocurrió un error probablemente peligroso, pero no dejó al Motor en un estado inestable. Si no se captura el error mediante un gestor definido por el usuario (vea también set_error_handler()), la aplicación se abortará como si fuera un E_ERROR.
 * E_DEPRECATED: 		Avisos en tiempo de ejecución. Habilítelo para recibir avisos sobre código que no funcionará en futuras versiones.
 * E_USER_DEPRECATED: 	Mensajes de advertencia generados por el usuario. Son como un E_DEPRECATED, excepto que es generado por código de PHP mediante el uso de la función de PHP trigger_error().
 * E_ALL: 				Todos los errores y advertencias soportados, excepto del nivel E_STRICT antes de PHP 5.4.0.
 * 
 */
$GLOBALS['C_CONFIG']['C_SYSTEM_ERROR'] = array(
	'E_ERROR' => 1,
	'E_WARNING' => 1,
	'E_PARSE' => 1,
	'E_NOTICE' => 1,
	'E_CORE_ERROR' => 1,
	'E_CORE_WARNING' => 1,
	'E_COMPILE_ERROR' => 1,
	'E_COMPILE_WARNING' => 1,
	'E_USER_ERROR' => 1,
	'E_USER_WARNING' => 1,
	'E_USER_NOTICE' => 1,
	'E_STRICT' => 1,
	'E_RECOVERABLE_ERROR' => 1,
	'E_DEPRECATED' => 1,
	'E_USER_DEPRECATED' => 1,
	'E_ALL' => 1
);
?>