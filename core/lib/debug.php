<?php

class debug
{
	/**
	 * Devuelve un arreglo con todos los archivos que se incluyeron
	 * 
	 * @return array
	 */
	static public function includes()
	{
		return get_included_files();
	}
	
	
	/**
	 * Devuelve un arreglo con la configuración
	 */
	static public function config()
	{
		return get_config();
	}
	
	
	/**
	 * Devuelve la memoria usada hasta el momento
	 * 
	 * Se puede especificar la unidad en que es devuelto el valor especificándolo como parámetro
	 * Parámetros soportados: "kb" "mb" y "gb"
	 *
	 * @param string $unidad
	 * @return number
	 */
	static public function memory($unidad=false)
	{
		$memoria = memory_get_usage(true);
		
		if ($unidad)
		{
			if ($unidad == 'kb')
				$memoria = $memoria / 1024;
			
			else if ($unidad == 'mb')
				$memoria = ($memoria / 1024) / 1024;
			
			else if ($unidad == 'gb')
				$memoria = (($memoria / 1024) / 1024) / 1024;
		}
		
		return $memoria;
	}
}

?>