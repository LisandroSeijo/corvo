<?php

class log
{
	/**
	 * Lista logs
	 * 
	 * @var lista_items
	 */
	static private $log;
	
	
	/**
	 * Inicializa la clase
	 */
	static public function inicializar()
	{
		self::$log = new lista_items();
	}
	
	
	/**
	 * Agrega un log
	 * 
	 * @param string $msg
	 * @param string $tipo
	 */
	static public function addLog($msg,$tipo=false)
	{
		$logitem = new log_item();
		
		$logitem->setMsg($msg);
		if ($tipo)
			$logitem->setTipo($tipo);
		
		self::$log->agregar($logitem);
	}
	
	
	/**
	 * Devuelve un log de una posición específica
	 * 
	 * @param int $x
	 * @return log_item
	 */
	static public function getLog($x)
	{
		return self::$log->getItem($x);
	}
	
	
	/**
	 * Devuelve la lista de logs
	 * 
	 * @return lista_items
	 */
	static public function getLogs()
	{
		return self::$log;
	}
	
	
	/**
	 * Devuelve un log en una posición específica
	 * 
	 * @param int $x
	 * @param string $tipo
	 */
	static public function getLogTipo($x,$tipo)
	{
		$ret = false;

		while($r=self::$log->siguiente())
		{
			if (strtolower($r->getTipo()) == strtolower($tipo))
			{
				$ret = $r;
				break;
			}
		}
		
		return $ret;
	}
	
	
	/**
	 * Devuelve una lista de logs de un tipo especpífico
	 * 
	 * @return lista_items
	 */
	static public function getLogsTipo($tipo)
	{
		$ret = new lista_items();
		
		while($r=self::$log->siguiente())
		{
			if (strtolower($r->getTipo()) == strtolower($tipo))
			{
				$ret->agregar($r);
			}
		}
		
		return $ret;
	}
}

?>