<?php

class db_mysqli extends DB
{
	public function connect($host=false, $user=false, $pass=false, $db=false)
	{
		if ($host !== false)
			$this->host = $host;
		
		if ($user !== false)
			$this->user = $user;
		
		if ($pass !== false)
			$this->pass = $pass;
		
		if ($db !== false)
			$this->db = $db;
		
		if (!$this->conexion = mysqli_connect($this->host, $this->user, $this->pass))
		{
			$this->error_msg = 'Error al conectar la base de datos';
			return 0;
		}

		if (!mysqli_select_db($this->conexion, $this->db))
		{
			$this->error_msg = "Error al seleccionar la base de datos: {$this->db}";
			return 0;
		}
		
		return 1;
	}
	
	public function disconnect()
	{
		if (!mysqli_close($this->conexion))
		{
			$this->error_msg = 'Error al desconectar la base de datos';
			return 0;
		}
	}
	
	public function query($query)
	{
		if (!$res=mysqli_query($query, $this->conexion))
		{
			$this->error_msg = "Hubo un error al generar la consulta: <strong>$query</strong>";
			return 0;
		}
		
		$this->res = $res;
		return $res;
	}
	
	public function fetch_array($res=false)
	{
		if ($res)
			return mysqli_fetch_array($res);
		else
			return mysqli_fetch_array($this->res);
	}
	
	public function error()
	{
		return mysqli_error($this->conexion);
	}
}

?>