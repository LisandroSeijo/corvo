<?php

class log_item
{
	/**
	 * Mensaje
	 * 
	 * @var string
	 */
	private $msg;
	
	
	/**
	 * Tipo del mensaje
	 * @var string
	 */
	private $tipo;
	
	
	/**
	 * Setea el mensaje del log
	 * 
	 * @param string $val
	 */
	function setMsg($val)
	{
		$this->msg = $val;
	}
	
	
	/**
	 * Setea el tipo de log
	 * 
	 * @param string $val
	 */
	function setTipo($val)
	{
		$this->tipo = $val;
	}
	
	
	/**
	 * Devuelve el mensaje del log
	 */
	function getMsg()
	{
		return $this->msg;
	}
	
	
	/**
	 * Devuelve el tipo de log
	 */
	function getTipo()
	{
		return $this->tipo;
	}
}

?>