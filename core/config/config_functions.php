<?php
/**
 * Devuelve un arreglo con todas las confugiraciones
 * 
 * @return unknown
 */
function get_config()
{
	global $C_CONFIG;
	return $C_CONFIG;
}


/**
 * Devuelve si las configuraciones están activas
 * 
 * @return boolean
 */
function get_config_activo()
{
	return get_config_index('ACTIVO');
}


/**
 * Devuelve una variable global de configuración
 * 
 * @param string $tipo
 * @param string $index
 * @return mixed
 */
function get_config_index($tipo, $sub=false)
{
	global $C_CONFIG;
	
	if (!isset($C_CONFIG[$tipo]) || ($sub && !isset($C_CONFIG[$tipo][$sub])))
		return;
	
	if (!$sub)
		return $C_CONFIG[$tipo];
	
	else
		return $C_CONFIG[$tipo][$sub];
}


/**
 * Devuelve una variable global de configuración mediante su index numérico
 * 
 * @param int $index
 * @param int $sub
 * @return mixed
 */
function get_config_numeric($index, $sub=false)
{
	global $C_CONFIG;
	$auxconf = array_values($C_CONFIG);
	
	if (!is_numeric($index) || ($sub && !is_numeric($sub)))
		return;
	
	if ($index >= count($auxconf) || $index < 0)
		return;
	
	if (!$sub)
		return $C_CONFIG[$index];
	
	else
	{
		$auxsub = array_values($auxconf[$index]);
		return $auxsub[$sub];
	}
}

?>