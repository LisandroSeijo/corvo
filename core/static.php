<?php
/**
 * Inicializa la base de datos
 */
if (get_config_activo())
{
	if (!database::inicializar())
	{
		if (get_config_activo() && get_config_index('C_DB','STRICT'))
		{
			die('Error al conectar la base de datos');
		}
	}
}


/**
 * Manejo de logs
 */
log::inicializar();
?>