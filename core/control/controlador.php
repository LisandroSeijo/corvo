<?php
/**
 * Clase padre de los controladores
 */
abstract class Controlador
{
	protected $modelo;
	protected $vista;

	public function Controlador()
	{
		$this->modelo = new modelo();
		$this->vista = new vista();
	}
}
?>