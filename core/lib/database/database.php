<?php
class database
{
	static public $db;
	
	
	static private $motor;


	static private $motores = array(
		'mysql'
	);
	
	
	static private $motor_default = 'mysql';
	
	
	static private $libreria;
	
	
	static private $librerias = array(
		'mysql',
		'mysqli',
		'pdo'
	);
	
	
	static private $libreria_default = 'mysqli';
	
	
	static private $error = 0;
	
	
	static private $error_msg;
	
	
	static public function inicializar($host=false, $user=false, $pass=false, $db=false, $libreria=false)
	{
		if (get_config_activo())
		{
			global $C_DB;
			
			if (!isset($C_DB) || empty($C_DB))
				return 0;
			
			$libreria = $C_DB['LIBRERIA'];
			$host = $C_DB['HOST'];
			$user = $C_DB['USER'];
			$pass = $C_DB['PASS'];
			$db = $C_DB['DB'];
		}
		
		else
		{
			$error = '';

			if (!$host)
				$error .= 'No se especificó el host<br />';
			if (!$user)
				$error .= 'No se especificó el user<br />';
			if ($pass)
				$error .= 'No se especificó la contraseña<br />';
			if ($db)
				$error .= 'No se especificó el nombre de la base de datos';

			if (!empty($error))
			{
				self::error($error);
				return 0;
			}
			
			$libreria = (empty($libreria)) ? self::$libreria_default : $libreria;
		}
		
		
		if (empty($libreria) || !in_array($libreria, self::$librerias))
			return 0;
		
		if ($motor == 'mysql')
			self::$db = new db_mysql();
			
		else if ($motor == 'mysqli')
			self::$db = new db_mysqli();
		
		self::$libreria = $libreria;
		self::$db->setHost($host);
		self::$db->setUser($user);
		self::$db->setPass($pass);
		self::$db->setDB($db);
		
		
		return self::$db->connect();
	}
	
	
	static private function error($msg)
	{
		self::$error = 1;
		self::$error_msg = $msg;
	}
	
	
	static public function getError()
	{
		return self::$error;
	}
	
	
	static public function getErrorMsg()
	{
		return self::$error_msg;
	}
}
?>