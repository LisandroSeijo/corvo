<?php
if (ruta::getCountParams())
{
	$ncon = ruta::getParam(0);
	
	$include = $path_control. $ncon .'.php';
}

else
{
	$ncon = 'cindex';
	$include = $path_control.'cindex.php';
}

if (!file_exists($include)) die('Error al cargar el controlador');

include $include;

if (!class_exists($ncon))
	die('Error al cargar el controlador');

$controlador = new $ncon();
?>