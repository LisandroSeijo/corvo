<?php
/**
 * Base de datos
 * 
 * Contiene la información básica de la configuración de la base de datos
 * 
 * Parámetros:
 * 
 * 'HOST': Nombre/ip del host de la base de datos
 * 
 * 
 * 'USER': Usuario de la base de datos
 * 
 * 
 * 'PASS': Constraseña de la base de datos 
 * 
 * 
 * 'DB': Nombre de la base de datos
 * 
 * 'STRICT': Indica si la ejecución debe finalizar si la base de datos no puede ser conectada
 * 
 * 'MOTOR': Motor de base de datos que a la que se va a conectar
 * 	Por el momento solo soporta "mysql"
 * 			
 * 	Opciones: 'mysql'
 * 
 * 
 * 'LIBRERIA': Librería php que se va a usar para las consultas
 * 				
 * 	Opciones: 
 * 'mysql'
 * 'mysqli'
 * 'pdo'
 * 
 */
$GLOBALS['C_CONFIG']['C_DB'] = array(
		'HOST' => 'localhost',
		'USER' => 'root',
		'PASS' => 'lisandro',
		'DB' => 'heladeria',
		'STRICT' => true,
		'MOTOR' => 'mysql',
		'LIBRERIA' => 'mysqli'
);
?>