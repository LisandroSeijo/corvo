<?php

/**
 * Clase de manejo de listas
 *
 * Contiene las funciones básicas para el manejo de listas
 *
 * Ejemplo:
 *
 * <code>
 * $lista = new lista_items();
 * $lista->agregar("un string");
 * </code>
 *
 * @package Listas
 */
class lista_items
{
	/**
	 * Arreglo que contiene la lista
	 * 
	 */
	private $items = array();
	

	/**
	 * Índice en el que está posicionado
	 * 
	 */
	private $indice_actual = 0;
	

	/**
	 * Indica si hay un error
	 * 
	 */
	private $error=0;

	/**
	 * Mensaje del error
	 * 
	 */
	private $error_msg=0;


	/**
	 * Agrega un nuevo item a la lista
	 *
	 * @return void
	 */
	function agregar($item)
	{
		$this->items[] = $item;
	}
	
	
	/**
	 * Agrega una lista de items
	 * 
	 * @param array|Object lista_items $items
	 */
	function agregar_items($items)
	{
		$ret = true;

		if (is_array($items))
		{
			array_push($this->items, $items);
		}
		
		else if ($items instanceof self)
		{
			while ($r=$items->siguiente())
				$this->agregar($r);
		}
		
		else
			$ret = false;
		
		return $ret;
	}


	/**
	 * Devuelve la cantidad de items que hay en la lista
	 *
	 * @return int
	 */
	function getCount()
	{
		return count($this->items);
	}

	
	/**
	 * Devuelve el item de una posicion especifica
	 *
	 * @param int posición del item
	 * @return item
	 */
	function getItem($x)
	{
		if ($x > $this->getCount() || $x < 0)
			return 0;

		return $this->items[$x];
	}
	
	
	/**
	 * Devuelve el item en la posicion que esta el indice actual
	 *
	 * @return item
	 */
	function getItemActual()
	{
		$ret = $this->items[$this->indice_actual];
		
		if (empty($ret))
			return 0;
		
		return $ret;
	}
	
	
	/**
	 * Devuelve el indice actual
	 *
	 * @return int indice
	 */
	function getIndice()
	{
		return $this->indice_actual;
	}
	
	
	/**
	 * Devuelve el arreglo con todos los items
	 *
	 * @return array items
	 */
	function getItems()
	{
		return $this->items;
	}
	
	
	/**
	 * Devuelve el siguiente elemento del arreglo
	 *
	 * @return item
	 * @return 0 si llegó al final
	 */
	function siguiente()
	{
		if ($this->indice_actual >= $this->getCount())
			return 0;

		return $this->items[$this->indice_actual++];
	}
	
	
	/**
	 * Devuelve el elemento anterior del arreglo
	 *
	 * @return item
	 * @return 0 si llegó al principio
	 */
	function anterior()
	{
		if ($this->indice_actual < 0)
			return 0;
		
		return $this->items[$this->indice_actual--];
	}
	
	
	/**
	 * Resetea el indice al inicio
	 *
	 * @return void
	 */
	function resetear_indice()
	{
		$this->indice_actual = 0;
	}
	
	
	/**
	 * Pone el indice actual en el ultimo lugar
	 *
	 * @return void
	 */
	function fin_indice()
	{
		$this->indice_actual = $this->getCount()-1;
	}
}

?>