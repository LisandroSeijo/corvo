<?php

abstract class DB
{
	/**
	 * Host de la base de datos
	 * @var string
	 */
	protected $host;
	
	
	/**
	 * Usuario de la base de datos
	 * 
	 * @var string
	 */
	protected $user;
	
	
	/**
	 * Contraseña de la base de datos
	 * 
	 * @var string
	 */
	protected $pass;
	
	
	/**
	 * Nombre de la base de datos
	 * 
	 * @var string
	 */
	protected $db;
	
	
	/**
	 * Link de la conexión
	 * 
	 * @var MySQL link|boolean
	 */
	protected $conexion = 0;
	
	
	/**
	 * Resultado de una consulta
	 * 
	 * @var resource
	 */
	protected $res = 0;
	
	
	/**
	 * Contiene un mensaje de error
	 * 
	 * @var string
	 */
	protected $error_msg;

	
	
	/**
	 * Contecta a la base de datos
	 * 
	 * @param string $host
	 * @param string $user
	 * @param string $pass
	 * @param string $db
	 * 
	 * @return boolean
	 */
	abstract protected function connect($host=false, $user=false, $pass=false, $db=false);
	
	
	/**
	 * Desconecta la base de datos
	 * 
	 * @return boolean
	 */
	abstract protected function disconnect();
	
	
	/**
	 * Envía una consulta
	 * 
	 * @param string $query
	 * @return boolean
	 */
	abstract protected function query($query);
	
	
	/**
	 * Devuelve un arreglo con el resultado actual y mueve el puntero hacia adeltante
	 * 
	 * @param resource $res
	 * @return mixed
	 */
	abstract protected function fetch_array($res);
	
	
	/**
	 * Devuelve si hubo un error
	 * 
	 * @return mixed
	 */
	abstract protected function error();
	
	
	
	
	/***************************************
	 * Métodos set y get de las propiedades
	 **************************************/
	
	/*
	 * set
	 */
	
	/**
	 * Setea el host de la base de datos
	 * 
	 * @param string $val
	 */
	public function setHost($val)
	{
		$this->host = $val;
	}
	
	
	/**
	 * Setea el usuario de la base de datos
	 * 
	 * @param string $val
	 */
	public function setUser($val)
	{
		$this->user = $val;
	}
	
	
	/**
	 * Setea la contraseña de la base de datos
	 * 
	 * @param string $val
	 */
	public function setPass($val)
	{
		$this->pass = $val;
	}
	
	
	/**
	 * Setea el nombre de la base de datos
	 * 
	 * @param string $val
	 */
	public function setDB($val)
	{
		$this->db = $val;
	}
	
	
	
	/*
	 * get
	 */
	/**
	 * Setea el host de la base de datos
	 *
	 * @param string $val
	 */
	public function getHost()
	{
		return $this->host;
	}
	
	
	/**
	 * Setea el usuario de la base de datos
	 *
	 * @param string $val
	 */
	public function getUser()
	{
		return $this->user;
	}
	
	
	/**
	 * Setea la contraseña de la base de datos
	 *
	 * @param string $val
	 */
	public function getPass()
	{
		return $this->pass;
	}
	
	
	/**
	 * Setea el nombre de la base de datos
	 *
	 * @param string $val
	 */
	public function getDB()
	{
		return $this->db;
	}
	
	
	/**
	 * Devuelve un mensaje de error
	 *
	 * @return string
	 */
	public function getErrorMsg()
	{
		return $this->error_msg;
	}
}

?>