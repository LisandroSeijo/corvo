<?
/**
 * Activa las configuraciones
 * 
 * Si está en true, usa las configuraciones del sistema
 * Si está en off, lo saltea
 * 
 * Al ponerlo en off algunos elementos como la clase de manejo base de datos deben ser inicializados con todos los datos
 * (ver /lib/database/DB.php y /lib/database/database.php)
 * 
 * Los reportes de errores de php (@link http://php.net/manual/es/function.error-reporting.php) 
 * también deben ser seteados o se usará la configuración por defecto de php
 * 
 * Las variables que contienen las carpetas donde están los modelos, vistas, controladores y librería
 * asi como las clases estáticas son indistintas al uso de la configuración
 */
$GLOBALS['C_CONFIG']['ACTIVO'] = true;
?>