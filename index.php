<?php
include "core/arranque.php";


/*
 * Carpeta donde se guardan las librerias con funciones
 */
$path_lib = 'include/';


/*
 * Carpeta que almacena los controladores 
 */
$path_control = 'control/';


/*
 * Carpeta que guarda los modelos
 */
$path_modelo = 'modelo/';


/*
 * Car�ta que guarda las vistas
 */
$path_vista = 'vista/';



/*
 * Se verifica que las carpetas existan
 */
$paths = array('include' => $path_lib, 'control' => $path_control, 'modelo' => $path_modelo);

foreach($paths as $tipo => $carpeta)
{
	if (!file_exists($carpeta)) die('La carpeta que almacena los archivos <b>'.$tipo.'</b> no fue encontrada');
}


/*
 * Seteo del timezone, previniendo el Warining para versiones 5.3 o superiores de php 
 */
date_default_timezone_set('America/Argentina/Buenos_Aires');


/*
 * Carga el controlador
 */
include "$C_PATH/cargar_controlador.php";
?>