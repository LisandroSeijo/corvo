<?php

class db_mysql extends DB
{
	function __construct()
	{
		parent::__construct();	
	}
	
	public function connect($host=false, $user=false, $pass=false, $db=false)
	{
		if ($host !== false)
			$this->host = $host;
		
		if ($user !== false)
			$this->user = $user;
		
		if ($pass !== false)
			$this->pass = $pass;
		
		if ($db !== false)
			$this->db = $db;
		
		if (!$this->conexion = mysql_connect($this->host, $this->user, $this->pass))
		{
			$this->error_msg = 'Error al conectar la base de datos';
			return 0;
		}
		
		if (!mysql_select_db($this->db, $this->conexion))
		{
			$this->error_msg = 'Error al seleccionar la base de datos';
			return 0;
		}
		
		return 1;
	}
	
	public function disconnect()
	{
		if (!mysql_close($this->conexion))
		{
			$this->error_msg = 'Error al desconectar la base de datos';
			return 0;
		}
	}
	
	public function query($query)
	{
		if (!$res=mysql_query($query, $this->conexion))
		{
			$this->error_msg = "Hubo un error al generar la consulta: <strong>$query</strong>";
			return 0;
		}
		
		$this->res = $res;
		return $res;
	}
	
	public function fetch_array($res=false)
	{
		if ($res)
			return mysql_fetch_array($res);
		else
			return mysql_fetch_array($this->res);
	}
	
	public function error()
	{
		return mysql_error($this->conexion);
	}
}

?>