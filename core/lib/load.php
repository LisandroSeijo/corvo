<?php
function loadclass($path,$class)
{
	$file = "$path.$class.php";
	
	if (!is_file($file))
		return 0;
	
	$ret = new $class();
	
	return $ret;
}