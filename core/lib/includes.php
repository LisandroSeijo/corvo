<?php
/**
 * Incluye todos los archivos PHP de una carpeta
 *
 * Ej: include_path('lib/paquete_de_clases/')
 *
 * @param string $path carpeta que contiene los archivos
 * @param bool $recursivo si se pasa como true incluye los archivos de las carpetas que tenga adentro
 * @return void
 */
function include_path($path,$recursivo=false)
{
	if (!is_dir($path))
		return 0;

	$path_open = opendir($path);

	while ($archivo = readdir($path_open))
	{
		if ("$path/$archivo" == "$path/".basename(__FILE__))
			continue;
		

		if ($recursivo == true && is_dir("$path/$archivo") && $archivo != '.' && $archivo != '..')
			include_path("$path/$archivo", true);
		

		if (extencion($archivo) == 'php')
		{
			require_once("$path/$archivo");
		}
	}

	return 1;
}

/**
 * Devulve la extención de un archivo
 *
 * @param string $archivo archivo
 * @return string $ret extencion del archivo
 */
function extencion($archivo)
{
	$sep = explode('.', $archivo);

	return $sep[count($sep)-1];
}
?>