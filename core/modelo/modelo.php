<?php

class modelo
{
	/**
	 * Carpeta de los modelos
	 * @var string
	 * @access private
	 */
	private $path;
	
	
	/**
	 * Constructor
	 */
	function __construct()
	{
		global $path_modelo;
		$this->path = $path_modelo;
	}
	
	
	/**
	 * Carga los modelos de un directorio o un archivo único
	 * 
	 * @param string $modelo
	 * @return mixed
	 */
	function cargar($modelo)
	{
		if (is_dir("$this->path/$modelo"))
		{
			include_path("$this->path/$modelo");
		}
		
		else if (is_file("$this->path/$modelo.php"))
		{
			include "$this->path/$modelo.php";
		}
		
		else
			return 0;
	}
}

?>